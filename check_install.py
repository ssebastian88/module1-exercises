import sys

print 'Python version is %s' % sys.version

try:
    import pytest
    print 'pytest version %s' % pytest.__version__
except ImportError:
    print 'pytest missing'

try:
    import IPython
    print 'ipython version %s' % IPython.__version__
except ImportError:
    print 'ipython missing'

try:
    import ttk
    print 'ttk version %s' % ttk.__version__
except ImportError:
    print 'ttk missing'

